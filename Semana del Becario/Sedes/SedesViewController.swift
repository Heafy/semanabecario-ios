//
//  SedesViewController.swift
//  Semana del Becario

import UIKit
import SwiftyJSON

/// Controller de la sección Sedes
class SedesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var sedesList = [Sede]()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        
        requestSedes()
    }
    
    // Prepare segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Segue para mostrar los detalles de la Sede
        if segue.identifier == VerSedeViewController.identifierSegue {
            let destination = segue.destination as! VerSedeViewController
            destination.idSede = sender as? Int
        }
    }
}

// MARK: - Table View Data Source
extension SedesViewController: UITableViewDataSource {
    
    // Numero de filas por sección
    // Secciones: 1, Filas: Tamaño de sedesList.sedes
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sedesList.count
    }
    
    // Asigna el contenido a cada celda dependiendo del indexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SedesTableViewCell.identifierCell, for: indexPath) as! SedesTableViewCell
        let sede = sedesList[indexPath.row]
        cell.poblar(with: sede)
        return cell
    }
}

// MARK: - Table View Delegate
extension SedesViewController: UITableViewDelegate {
    
    // Crea el segue cuando una cell es seleccionada
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sede = sedesList[indexPath.row]
        performSegue(withIdentifier: VerSedeViewController.identifierSegue, sender: sede.id)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - API Request
extension SedesViewController {
    
    /// Función que ejecuta el Request
    func requestSedes() {
        let session = URLSession.shared
        let requestURLString = APIService.sedes
        guard let requestURL = URL(string: requestURLString) else { return }
        
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            if let error = error {
                debugPrint(error)
            }
            
            if let data = data {
                do {
                    let jsonSedes = try JSONDecoder().decode([Sede].self, from: data)
                    self.sedesList = jsonSedes
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }catch{
                    debugPrint("Error during JSON DataSerialization from Response: \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
}
