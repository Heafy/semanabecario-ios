//
//  VerSedeViewController.swift
//  Semana del Becario

import UIKit
import CoreLocation
import MapKit

/// Controller de la sección Sedes
class VerSedeViewController: UIViewController{
    
    static let identifierSegue = "segueVerSede"
    
    @IBOutlet weak var sedeImageView: UIImageView!
    @IBOutlet weak var siglasLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var direccionLabel: UILabel!
    @IBOutlet weak var aulasTitleLabel: UILabel!
    @IBOutlet weak var listaAulasLabel: UILabel!
    @IBOutlet weak var mapaTitleLabel: UILabel!
    @IBOutlet weak var croquisImageView: UIImageView!
    
    var idSede: Int?
    var sede: Sede?

     // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //decorate()
        setup()
    }
    
    override func loadView() {
        super.loadView()
        debugPrint("ID Sede: \(idSede ?? 0)")
        requestSede(idSede: idSede ?? 1)
    }
    
    // Muestra los elementos del view
    func decorate() {
        self.title = sede?.siglas
        siglasLabel.text = sede?.siglas
        nombreLabel.text = sede?.nombreSede
        direccionLabel.text = sede?.direccion
        listaAulasLabel.text = sede?.listaAulas
        // Carga una imagen desde la URL
        if sede?.rutaImg != "" {
            sedeImageView.load(string: (sede?.getRutaIMG())!)
        }
        if sede?.rutaCroquis != "" {
            croquisImageView.load(string: (sede?.getRutaCroquis())!)
        }
    }
    
    // Configura metodos adicionales
    func setup() {
        // Hace que direccion label se pueda presionar
        let direccionLabelTap = UITapGestureRecognizer(target: self, action: #selector(direccionLabelPressed(_:)))
        direccionLabel.isUserInteractionEnabled = true
        direccionLabel.addGestureRecognizer(direccionLabelTap)
    }
    
    // Función que se ejecuta cuando direccionLabel es presionada
    @objc func direccionLabelPressed(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "segueMaps", sender: nil)
    }
    
    // Metodo para convertir una cadena en coordenada
    func coordinates(forAddress address: String, completion: @escaping (CLLocationCoordinate2D?) -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) {
            (placemarks, error) in
            guard error == nil else {
                print("Geocoding error: \(error!)")
                completion(nil)
                return
            }
            completion(placemarks?.first?.location?.coordinate)
        }
    }
    
    /// Método para abrir una coordenada en Apple Maps
    /// - Parameters:
    ///   - lat: Latitud
    ///   - long: Longitud
    ///   - placeName: El nombre que tenga la coordenada
    func openMapForPlace(lat:Double = 0, long:Double = 0, placeName:String = "") {
        let latitude: CLLocationDegrees = lat
        let longitude: CLLocationDegrees = long

        let regionDistance:CLLocationDistance = 100
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = placeName
        mapItem.openInMaps(launchOptions: options)
    }
}

extension VerSedeViewController {
    
    func requestSede(idSede: Int) {
        let session = URLSession.shared
        let requestURLString = APIService.detalleSede
        guard let requestURL = URL(string: requestURLString + "\(idSede)") else { return }
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            
            if let error = error {
                debugPrint(error)
            }
            
            if let data = data {
                do {
                    let sede = try JSONDecoder().decode(Sede.self, from: data)
                    self.sede = sede
                    DispatchQueue.main.async {
                        self.decorate()
                    }
                }catch{
                    debugPrint("Error during JSON DataSerialization from Response: \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueMaps" {
            let destinationController = segue.destination as! ViewControllerMaps
            destinationController.textoTitulo = siglasLabel.text!
        }
    }
    
}


