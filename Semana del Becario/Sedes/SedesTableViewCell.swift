//
//  SedesTableViewCell.swift
//  Semana del Becario

import UIKit

/// Clase para modelar una Celda de la sección Sedes
class SedesTableViewCell: UITableViewCell {
    
    static let identifierCell = "sedeCell"
    
    @IBOutlet weak var siglasLabel: UILabel!
    @IBOutlet weak var nombreLabel: UILabel!
    @IBOutlet weak var direccionLabel: UILabel!
    @IBOutlet weak var sedeImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // Llena los datos de la Cell con la Actividad dada
    func poblar(with sede: Sede) {
        siglasLabel.text = sede.siglas
        nombreLabel.text = sede.nombreSede
        direccionLabel.text = sede.direccion
        // Carga una imagen desde la URL
        if sede.rutaImg != "" {
            sedeImageView.load(string: sede.getRutaIMG())
        }
    }
    
}

// MARK: - Extension UIImageView
// Método para cargar una imagen desde la URL
extension UIImageView {
    
    func load(string: String) {
        let url = URL(string: string)
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url!) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
