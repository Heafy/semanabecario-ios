//
//  ViewControllerMaps.swift
//  Semana del Becario
//
//  Created by Gabo on 15/04/21.
//  Copyright © 2021 DGTIC. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewControllerMaps: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate{
    @IBOutlet var map: MKMapView!
    var locationManager = CLLocationManager()
    
    var textoTitulo = String()
    var ubicacion = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if textoTitulo == "DGTIC" {
            ubicacion = "19.322767, -99.184733"
        }else if textoTitulo == "IIMAS"{
            ubicacion = "19.330056, -99.180748"
        }else if textoTitulo == "FCA"{
            ubicacion = "19.324104, -99.184693"
        }
        print("titulo devuelto de vista pasada" + textoTitulo)
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        map.delegate = self
        
        getAddress()
        
    }
    
    func getAddress() {
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(
            ubicacion){ (placemarks,error)
            in
            guard let placemarks = placemarks,let location = placemarks.first?.location
                else{
                    print("No location Found")
                    return
            }
            print(location)
            self.mapThis(destinationCord: location.coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
    func mapThis(destinationCord : CLLocationCoordinate2D) {
        let souceCordinate = (locationManager.location?.coordinate)!
        let soucePlaceMark = MKPlacemark(coordinate: souceCordinate)
        let destPlaceMark = MKPlacemark(coordinate: destinationCord)
        let sourceItem = MKMapItem(placemark: soucePlaceMark)
        let destItem = MKMapItem(placemark: destPlaceMark)
        let destinationRequest = MKDirections.Request()
        destinationRequest.source = sourceItem
        destinationRequest.destination = destItem
        destinationRequest.transportType = .automobile
        destinationRequest.requestsAlternateRoutes = true
        
        let directions = MKDirections(request: destinationRequest)
        directions.calculate{(response,error)in
            guard let response = response else{
                if let error = error{
                    print("Something is wrong")
                }
                return
            }

            let route = response.routes[0]
            self.map.addOverlay(route.polyline)
            self.map.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let render = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        render.strokeColor = .systemBlue
        return render
    }

}
