//
//  APIService.swift
//  Semana del Becario

import Foundation

struct APIService {
    
    static let baseURL = "http://becariosddtic.tic.unam.mx/"
    static let registro = baseURL + "asistente/registrar"
    static let actividades = baseURL + "actividades"
    static let verificarInscripcion = baseURL + "actividades/verificarInscripcion"
    static let inscribirActividad = baseURL + "actividades/inscribir"
    static let desinscribirActividad = baseURL + "actividades/desinscribir"
    static let detalleActividad = actividades + "/detalle/"
    static let detallePonente = baseURL + "ponente/obtenerPonente/"
    static let storagePonentes = baseURL + "storage/ponentes/"
    static let misActividades = baseURL + "asistentes/misActividades"
    static let sedes = baseURL + "sedes"
    static let detalleSede =  sedes + "/detalle/"
    static let storageSedes = baseURL + "storage/sedes/"
}
