//
//  ProgramaTableViewCell.swift
//  Semana del Becario
//
//  Created by Comunicacion03 on 5/12/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit

class ProgramaTableViewCell: UITableViewCell {

    static let identifierCell = "actividadCell"
    
    @IBOutlet weak var eventoLabel: UILabel!
    @IBOutlet weak var tipoLabel: UILabel!
    @IBOutlet weak var ponenteLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var horaLabel: UILabel!
    @IBOutlet weak var eventoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    // Llena los datos de la Cell con la Actividad dada
    func poblar(with actividad: ActividadLista) {
        eventoLabel.text = actividad.nombreActividad
        tipoLabel.text = actividad.nombreTipo
        ponenteLabel.text = actividad.ponente
        ponenteLabel.isHidden = true
        fechaLabel.text = actividad.getDate()
        horaLabel.text = actividad.getTime()
        eventoImageView.image = actividad.getImage()
    }
}
