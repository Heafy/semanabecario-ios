//
//  VerActividadViewController.swift
//  Semana del Becario


import UIKit

class VerActividadViewController: UIViewController {
    
    @IBOutlet weak var nombreEvento: UILabel!
    @IBOutlet weak var imageEvento: UIImageView!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var horaLabel: UILabel!
    @IBOutlet weak var sedeButton: UIButton!
    @IBOutlet weak var aulaLabel: UILabel!
    @IBOutlet weak var lugaresLabel: UILabel!
    
    @IBOutlet weak var tipoActividadLabel: UILabel!
    @IBOutlet weak var inscritoLabel: UILabel!
    
    @IBOutlet weak var ponentesStackView: UIStackView!
    @IBOutlet weak var ponentesStackViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var verEntradaButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    
    
    static let identifierSegue = "segueVerSede"
    
    var actividadActual: ActividadDetalle?
    var idActividad: Int = 0
    var inscrito: Bool = false
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        debugPrint("ID Actividad: \(idActividad )")
        requestActividad(idActividad: idActividad)
        let idAsistente = UserDefaults.standard.integer(forKey: "idAsistente")
        verificarInscripcion(idActividad: idActividad, idAsistente: idAsistente)
    }
    
    // Prepare segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Segue para mostrar los detalles de la Sede
        if segue.identifier == VerSedeViewController.identifierSegue {
            let destination = segue.destination as! VerSedeViewController
            destination.idSede = sender as? Int
        }
        // Segue para mostrar la información del ponente
        if segue.identifier == PonenteViewController.identifierSegue {
            let destination = segue.destination as! PonenteViewController
            destination.idPonente = sender as? Int
        }
        // Segue para mostrar la entrada del asistente
        if segue.identifier == VerEntradaViewController.identifierSegue {
            let destination = segue.destination as! VerEntradaViewController
            destination.idActividad = sender as? Int
        }
    }
    
    func populate(actividad: ActividadDetalle) {
        self.title = actividad.nombreTipo
        nombreEvento.text = actividad.nombreActividad
        tipoActividadLabel.text = actividad.nombreTipo
        imageEvento.image = actividad.getImage()
        fechaLabel.text = actividad.getDate()
        horaLabel.text = actividad.getTime()
        sedeButton.setTitle(actividad.nombreSede, for: .normal)
        aulaLabel.text = actividad.aula
        lugaresLabel.text = "\(actividad.lugaresRestantes)"
        descripcionLabel.text = actividad.descripcion
        createPonenteButtons()
    }
    
    func createPonenteButtons() {
        for ponente in actividadActual!.ponentes {
            let button = UIButton()
            button.setTitle("\(ponente.nombre) \(ponente.apellidoPaterno) \(ponente.apellidoMaterno)", for: .normal)
            button.setTitleColor(#colorLiteral(red: 0.7490196078, green: 0.568627451, blue: 0.2235294118, alpha: 1), for: .normal)
            button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
            button.frame.size =  CGSize(width: 250, height: 35)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.contentHorizontalAlignment = .left
            button.tag = ponente.id
            button.addTarget(self, action: #selector(ponenteButtonPressed), for: .touchUpInside)
            ponentesStackView.addArrangedSubview(button)
        }
        ponentesStackViewWidth.constant = CGFloat(35 * (actividadActual?.ponentes.count)!)
        view.layoutIfNeeded()
    }
    
    func setRegisteredInfo(isRegistered: Bool) {
        DispatchQueue.main.async {
            if isRegistered {
                self.inscrito = true
                self.inscritoLabel.text = "INSCRITO"
                self.inscritoLabel.textColor = .green
                self.registerButton.setTitle("Desinscribirse", for: .normal)
                self.verEntradaButton.isHidden = false
            } else {
                self.inscrito = false
                self.inscritoLabel.text = "NO INSCRITO"
                self.inscritoLabel.textColor = .red
                self.registerButton.setTitle("Inscribirse", for: .normal)
                self.verEntradaButton.isHidden = true
            }
        }
    }
    
    // MARK:- Redirect registro
    
    /// Función para redirigir al programa
    func routeToRegistro() {
        tabBarController?.selectedIndex = 1
    }
    // MARK:- IBActions
    
    @IBAction func sedeButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: VerSedeViewController.identifierSegue, sender: actividadActual?.idSede)
    }
    
    @objc func ponenteButtonPressed(sender: UIButton!) {
        performSegue(withIdentifier: PonenteViewController.identifierSegue, sender: sender.tag)
    }
    
    @IBAction func verEntradaButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: VerEntradaViewController.identifierSegue, sender: idActividad)
    }
    
    // MARK: Alert Inscribit/Desincribir
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        let idAsistente = UserDefaults.standard.integer(forKey: "idAsistente")
        if idAsistente == 0 {
            showAlertRegistrar()
        } else {
            showAlertInscribir(idAsistente: idAsistente)
        }
    }
    
    func showAlertRegistrar() {
        let alert = UIAlertController(title: "Aviso", message: "Para inscribirte a una actividad necesitas estar registrado.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Registrarme", style: .default, handler: {
            action in
            self.routeToRegistro()
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func showAlertInscribir(idAsistente: Int) {
        let title: String?
        let message: String?
        if !inscrito {
            title = "¿Deseas incribirte a esta actividad?"
            message = "Presiona aceptar para inscribirte."
        } else {
            title = "¿Deseas desincribirte de esta actividad?"
            message = "Presiona aceptar para desinscribirte."
        }
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: {
            action in
            if !self.inscrito {
                self.inscribirActividad(idActividad: self.idActividad, idAsistente: idAsistente)
            } else {
                self.desincribirActividad(idActividad: self.idActividad, idAsistente: idAsistente)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}

// MARK:- API Request
extension VerActividadViewController {
    
    // MARK: Request Info Actividad
    
    // Request datos de la actividad
    func requestActividad(idActividad: Int) {
        let session = URLSession.shared
        let requestURLString = APIService.detalleActividad
        guard let requestURL = URL(string: requestURLString + "\(idActividad)") else { return }
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if let error = error {
                debugPrint(error)
            }
            if let data = data {
                do {
                    self.actividadActual = try JSONDecoder().decode(ActividadDetalle.self, from: data)
                    DispatchQueue.main.async {
                        self.populate(actividad: self.actividadActual!)
                    }
                }catch{
                    debugPrint("Error during JSON Data List Decoding: \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
    // MARK: Verificar inscripción
    
    func verificarInscripcion(idActividad: Int, idAsistente: Int) {
        let session = URLSession.shared
        let requestURLString = APIService.verificarInscripcion
        guard let requestURL = URL(string: requestURLString) else { return }
        var dataArr = [String]()
        dataArr.append("idActividad=\(idActividad)")
        dataArr.append("idAsistente=\(idAsistente)")
        let dataStr = dataArr.map { String($0) }.joined(separator: "&")
        let data = dataStr.data(using: .utf8)
        var postRequest = URLRequest(url: requestURL)
        postRequest.httpMethod = "POST"
        postRequest.httpBody = data
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: postRequest, completionHandler: {
            (data, response, error) in
            if let error = error {
                debugPrint(error)
                self.setRegisteredInfo(isRegistered: false)
            }
            if let data = data {
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                if let dictionary = json as? [String: Any] {
                    if let bool = dictionary["respuesta"] as? Bool {
                        print("Verificar inscripción: \(bool)")
                        self.setRegisteredInfo(isRegistered: bool)
                    }
                }
            }
        })
        task.resume()
    }
    
    // MARK: Inscribir actividad
    
    func inscribirActividad(idActividad: Int, idAsistente: Int) {
        let session = URLSession.shared
        let requestURLString = APIService.inscribirActividad
        guard let requestURL = URL(string: requestURLString) else { return }
        var dataArr = [String]()
        dataArr.append("idActividad=\(idActividad)")
        dataArr.append("tokenAsistente=TODO: Token de Firebase")
        dataArr.append("idAsistente=\(idAsistente)")
        let dataStr = dataArr.map { String($0) }.joined(separator: "&")
        let data = dataStr.data(using: .utf8)
        var postRequest = URLRequest(url: requestURL)
        postRequest.httpMethod = "POST"
        postRequest.httpBody = data
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: postRequest) {
            (data, response, error) in
            
            if let error = error {
                debugPrint("Error")
                debugPrint(error)
                self.setRegisteredInfo(isRegistered: false)
            }
            
            if let data = data, let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    debugPrint("Actividad Inscrita")
                    self.setRegisteredInfo(isRegistered: true)
                } else {
                    debugPrint(String(decoding: data, as: UTF8.self))
                    self.setRegisteredInfo(isRegistered: false)
                }
            }
        }
        task.resume()
    }
    
    // MARK: Desincribir actividad
    
    func desincribirActividad(idActividad: Int, idAsistente: Int) {
        let session = URLSession.shared
        let requestURLString = APIService.desinscribirActividad
        guard let requestURL = URL(string: requestURLString) else { return }
        var dataArr = [String]()
        dataArr.append("idActividad=\(idActividad)")
        dataArr.append("idAsistente=\(idAsistente)")
        let dataStr = dataArr.map { String($0) }.joined(separator: "&")
        let data = dataStr.data(using: .utf8)
        var postRequest = URLRequest(url: requestURL)
        postRequest.httpMethod = "POST"
        postRequest.httpBody = data
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: postRequest) {
            (data, response, error) in
            
            if let error = error {
                debugPrint("Error")
                debugPrint(error)
                self.setRegisteredInfo(isRegistered: false)
            }
            
            if let data = data, let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    debugPrint("Actividad Desinscrita")
                    self.setRegisteredInfo(isRegistered: false)
                } else {
                    debugPrint(String(decoding: data, as: UTF8.self))
                    self.setRegisteredInfo(isRegistered: true)
                }
            }
        }
        task.resume()
    }
    
}

// TODO: Eliminar cosas hardcodeadas
// TODO: Probar un ciclo completo
