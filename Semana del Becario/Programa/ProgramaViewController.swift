//
//  FirstViewController.swift
//  Semana del Becario

import UIKit

/// Controller para la seccion Programa
class ProgramaViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var actividadesList = [ActividadLista]()
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        //tableView.estimatedRowHeight = 200
    }
    
    override func loadView() {
        super.loadView()
        
        requestActividades()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == VerActividadViewController.identifierSegue {
            let actividad = sender as! ActividadLista
            let destination = segue.destination as! VerActividadViewController
            destination.idActividad = actividad.idActividad
        }
    }
    
}

// MARK: - Table View Data Source
extension ProgramaViewController: UITableViewDataSource {
    
    // Numero de filas por sección
    // Secciones: 1, Filas: Tamaño de actividadesList.actividades
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actividadesList.count
    }
    
    // Asigna el contenido a cada celda dependiendo del indexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProgramaTableViewCell.identifierCell, for: indexPath) as! ProgramaTableViewCell
        let actividad = actividadesList[indexPath.row]
        cell.poblar(with: actividad)
        return cell
    }
}

// MARK: - Table View Delegate
extension ProgramaViewController: UITableViewDelegate {
        
    // Crea el segue cuando una cell es seleccionada
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let actividad = actividadesList[indexPath.row]
        performSegue(withIdentifier: VerSedeViewController.identifierSegue, sender: actividad)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - API Request
extension ProgramaViewController {
    
    /// Función que ejecuta el Request
    func requestActividades() {
        let session = URLSession.shared
        let requestURLString = APIService.actividades
        guard let requestURL = URL(string: requestURLString) else { return }
        
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if let error = error {
                debugPrint(error)
            }
            if let data = data {
                do {
                    self.actividadesList = try JSONDecoder().decode([ActividadLista].self, from: data)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }catch{
                    debugPrint("Error during JSON Data List Decoding: \(error)")
                }
            }
        })
        task.resume()
        
    }
}

