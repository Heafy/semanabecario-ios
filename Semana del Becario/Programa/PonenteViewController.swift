//
//  PonenteViewController.swift
//  Semana del Becario
//
//  Created by Jorge Martinez on 27/09/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit

class PonenteViewController: UIViewController {

    @IBOutlet weak var ponenteLabel: UILabel!
    @IBOutlet weak var ponenteImage: UIImageView!
    @IBOutlet weak var gradoAcademicoLabel: UILabel!
    @IBOutlet weak var semblanzaLabel: UILabel!
    
    static let identifierSegue = "segueVerPonente"
    
    var idPonente: Int?
    
    // MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        super.loadView()
        debugPrint("ID Ponente \(idPonente ?? 0)")
        requestPonente(idPonente: idPonente ?? 1)
    }
    
    func populate(ponente: Ponente) {
        ponenteLabel.text = "\(ponente.nombre) \(ponente.apellidoPaterno) \(ponente.apellidoMaterno)"
        gradoAcademicoLabel.text = ponente.gradoAcademico
        semblanzaLabel.text = ponente.semblanza
        ponenteImage.load(string: ponente.getImage())
    }
    
}

extension PonenteViewController {
    
    func requestPonente(idPonente: Int) {
        let session = URLSession.shared
        let requestURLString = APIService.detallePonente
        guard let requestURL = URL(string: requestURLString + "\(idPonente)") else { return }
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if let error = error {
                debugPrint(error)
            }
            if let data = data {
                do {
                    let ponente = try JSONDecoder().decode(Ponente.self, from: data)
                    DispatchQueue.main.async {
                        self.populate(ponente: ponente)
                        //self.populate(actividad: actividad)
                        //print(actividad.ponentes)
                    }
                }catch{
                    debugPrint("Error during JSON Data List Decoding: \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
    
}
