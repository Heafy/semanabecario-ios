//
//  OpcionMas.swift
//  Semana del Becario


import Foundation

/// Clase para modelar los objetos que representan una opcion en la sección Mas
struct Opcion {
    
    var icono: String
    var valor: String
    
    /// Constructor único
    /// - Parameters:
    ///   - icono: Nombre del ícono (imágenes libres de Apple)
    ///   - nombre: Valor que tendra la opcion
    init(icono: String, valor: String){
        self.icono = icono
        self.valor = valor
    }
    
}
