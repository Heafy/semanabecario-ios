//
//  Sede.swift
//  Semana del Becario

import Foundation

/// Clase para modelar los objetos instancia de una sede
struct Sede: Codable {
    
    var id: Int
    var siglas: String
    var nombreSede: String
    var direccion: String
    var ubicacion: String
    var listaAulas: String
    var rutaImg : String
    var rutaCroquis: String
    
    /// Convierte el valor obtenido de rutaImg en la URL en forma de cadena para obtener la imagen
    /// - Returns: La URL en forma de cadena para obtener la imagen
    func getRutaIMG() -> String {
        return APIService.storageSedes + rutaImg
    }
    
    /// Convierte el valor obtenido de rutaImg en la URL en forma de cadena para obtener la imagen
    /// - Returns: La URL en forma de cadena para obtener la imagen
    func getRutaCroquis() -> String {
        return APIService.storageSedes + rutaCroquis
    }
}
