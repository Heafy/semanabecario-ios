//
//  ActividadDetalle.swift
//  Semana del Becario
//
//  Created by Jorge Martinez on 23/08/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import Foundation
import UIKit

struct ActividadDetalle: Codable {

    var nombreActividad: String
    var nombreTipo: String
    var fechas: [Fecha]
    var idSede: Int
    var nombreSede: String
    var aula: String
    var cupo: Int
    var numeroInscritos: Int
    var ponentes: [PonenteDetalle]
    var descripcion: String
    var nombreImagen: String
    var linkCuestionario: String
    var lugaresRestantes: Int {
        get {
            return cupo - numeroInscritos
        }
    }
    var tipo: tipoEnum {
        get {
            switch nombreTipo {
            case "Conferencia":
                return .Conferencia
            case "Mesa Redonda":
                return .MesaRedonda
            case "Taller":
                return .Taller
            default:
                return .Actividad
            }
        }
    }
    
    private func createDate(from strFecha: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: strFecha)
    }
    
    func getDate() -> String {
        if fechas.count == 1 {
            let strFecha = fechas.first!.fecha
            let date = createDate(from: strFecha)
            return "\(date!.dayMedium) de \(date!.monthMedium)"
        } else {
            var resFecha = ""
            for fechaUnica in fechas {
                let strFecha = fechaUnica.fecha
                let date = createDate(from: strFecha)
                resFecha += "\(date!.dayMedium) de \(date!.monthMedium) - "
            }
            _ = resFecha.removeLast()
            _ = resFecha.removeLast()
            return resFecha
        }
    }
    
    func getTime() -> String {
        if fechas.count == 1 {
            let strFecha = fechas.first!.fecha
            let date = createDate(from: strFecha)
            return "\(date!.hour12):\(date!.minute0x) \(date!.amPM)"
        } else {
            var resHora = ""
             for fechaUnica in fechas {
                 let strFecha = fechaUnica.fecha
                 let date = createDate(from: strFecha)
                 resHora += "\(date!.hour12):\(date!.minute0x) \(date!.amPM) - "
             }
             _ = resHora.removeLast()
             _ = resHora.removeLast()
             return resHora
        }
    }
    
    func getImage() -> UIImage {
        let image: UIImage
        switch tipo {
        case .Conferencia:
            image = #imageLiteral(resourceName: "conferencia")
        case .MesaRedonda:
            image = #imageLiteral(resourceName: "MesaRedonda")
        case .Taller:
            image = #imageLiteral(resourceName: "Taller2")
        default:
            image = #imageLiteral(resourceName: "Varios")
        }
        return image
    }
}
    
