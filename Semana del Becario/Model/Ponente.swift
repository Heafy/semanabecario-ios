//
//  Ponente.swift
//  Semana del Becario

import Foundation

/// Clase para modelar los objetos instancia de un Ponente
struct Ponente: Codable {
    
    var id: Int
    var nombre: String
    var apellidoPaterno: String
    var apellidoMaterno: String
    var gradoAcademico: String
    var semblanza: String
    var nombreImagen: String
    
    init(id: Int, nombre: String, apellidoPaterno: String, apellidoMaterno: String, gradoAcademico: String, semblanza: String, nombreImagen: String) {
        self.id = id
        self.nombre = nombre
        self.apellidoPaterno = apellidoPaterno
        self.apellidoMaterno = apellidoMaterno
        self.gradoAcademico = gradoAcademico
        self.semblanza = semblanza
        self.nombreImagen = nombreImagen
    }
    
    /// Convierte el valor obtenido de rutaImg en la URL en forma de cadena para obtener la imagen
    /// - Returns: La URL en forma de cadena para obtener la imagen
    func getImage() -> String {
        return APIService.storagePonentes + nombreImagen
    }
}
