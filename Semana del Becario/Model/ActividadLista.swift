//
//  Actividad.swift
//  Semana del Becario

import Foundation
import UIKit

struct ActividadLista: Codable {
    
    var idActividad: Int
    var nombreActividad: String
    var ponente: String?
    var nombreTipo: String?
    var fecha: [Fecha]
    var siglas: String?
    var aula: String?
    var tipo: tipoEnum {
        get {
            switch nombreTipo {
            case "Conferencia":
                return .Conferencia
            case "Mesa Redonda":
                return .MesaRedonda
            case "Taller":
                return .Taller
            default:
                return .Actividad
            }
        }
    }
    
    func createDate(from strFecha: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: strFecha)
    }
    
    func getDate() -> String {
        if fecha.count == 1 {
            let strFecha = fecha.first!.fecha
            let date = createDate(from: strFecha)
            return "\(date!.dayMedium) de \(date!.monthMedium)"
        } else {
            var resFecha = ""
            for fechaUnica in fecha {
                let strFecha = fechaUnica.fecha
                let date = createDate(from: strFecha)
                resFecha += "\(date!.dayMedium) de \(date!.monthMedium) - "
            }
            _ = resFecha.removeLast()
            _ = resFecha.removeLast()
            return resFecha
        }
    }
    
    func getTime() -> String {
        if fecha.count == 1 {
            let strFecha = fecha.first!.fecha
            let date = createDate(from: strFecha)
            return "\(date!.hour12):\(date!.minute0x) \(date!.amPM)"
        } else {
            var resHora = ""
             for fechaUnica in fecha {
                 let strFecha = fechaUnica.fecha
                 let date = createDate(from: strFecha)
                 resHora += "\(date!.hour12):\(date!.minute0x) \(date!.amPM) - "
             }
             _ = resHora.removeLast()
             _ = resHora.removeLast()
             return resHora
        }
    }
    
    func getImage() -> UIImage {
        let image: UIImage
        switch tipo {
        case .Conferencia:
            image = #imageLiteral(resourceName: "conferencia")
        case .MesaRedonda:
            image = #imageLiteral(resourceName: "MesaRedonda")
        case .Taller:
            image = #imageLiteral(resourceName: "Taller2")
        default:
            image = #imageLiteral(resourceName: "Varios")
        }
        return image
    }
}

// MARK:- Extension Formatter

extension Formatter {
    static let dayMedium: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d"
        return formatter
    }()
    static let monthMedium: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "LLLL"
        formatter.locale = Locale(identifier: "es_MX")
        return formatter
    }()
    static let hour12: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh"
        return formatter
    }()
    static let minute0x: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "mm"
        return formatter
    }()
    static let amPM: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "a"
        return formatter
    }()
}

// MARK:- Extension Date

extension Date {
    var dayMedium: String   { return Formatter.dayMedium.string(from: self) }
    var monthMedium: String { return Formatter.monthMedium.string(from: self) }    //"May"
    var hour12:  String     { return Formatter.hour12.string(from: self) }         //"1"
    var minute0x: String    { return Formatter.minute0x.string(from: self) }       //"18"
    var amPM: String        { return Formatter.amPM.string(from: self) }           //"PM"
}
