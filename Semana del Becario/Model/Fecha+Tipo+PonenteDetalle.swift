//
//  Fecha+Tipo+PonenteDetalle.swift
//  Semana del Becario
//
//  Created by Jorge Martinez on 23/08/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import Foundation

enum tipoEnum: String {
    case Conferencia = "Conferencia"
    case MesaRedonda = "Mesa Redonda"
    case Taller = "Taller"
    case Actividad = "Actividad"
}

struct Fecha: Codable {
    var fecha: String
    var duracion: Int
}

struct PonenteDetalle: Codable {
    var id: Int
    var rol: String
    var nombre: String
    var apellidoPaterno: String
    var apellidoMaterno: String
}
