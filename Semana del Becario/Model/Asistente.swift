//
//  Asistente.swift
//  Semana del Becario

import Foundation

// MARK: - Asistente padre
/// Clase para modelar los objetos instancias de un Asistente
class Asistente {
    
    var id : Int
    var tipo : Int
    var nombre : String
    var apellidoPaterno : String
    var apellidoMaterno : String
    var correo : String
    var telefonoParticular : String
    var telefonoCelular : String
    
    /// Constructor único
    /// - Parameters:
    ///   - id: ID del asistente
    ///   - tipo: Tipo
    ///   - nombre: Nombre
    ///   - apellidoPaterno: Apellido paterno
    ///   - apellidoMaterno: Apellido materno
    ///   - correo: Correo
    ///   - telefonoParticular: Telefono particular
    ///   - telefonoCelular: Telefono celular
    init( id : Int, tipo : Int,  nombre : String, apellidoPaterno : String, apellidoMaterno : String, correo : String, telefonoParticular : String, telefonoCelular : String ){
        self.id = id
        self.tipo = tipo
        self.nombre = nombre
        self.apellidoPaterno = apellidoPaterno
        self.apellidoMaterno = apellidoMaterno
        self.correo = correo
        self.telefonoParticular = telefonoParticular
        self.telefonoCelular = telefonoCelular
    }
    
    /// Funcion para convertir un Asistente en un diccionario
    /// - Returns: El asistente en forma de diccionario
    func toDictionary() -> [String: Any] {
        var dictionary = [String: Any]()
        dictionary["idAsistente"] = id
        dictionary["tipoAsistente"] = tipo
        dictionary["nombre"] = nombre
        dictionary["apellidoPaterno"] = apellidoPaterno
        dictionary["apellidoMaterno"] = apellidoMaterno
        dictionary["correo"] = correo
        dictionary["telefonoParticular"] = telefonoParticular
        dictionary["telefonoCelular"] = telefonoCelular
        return dictionary
    }
    
}

// MARK: - Asistente Becario
/// Subclase para modelar los objetos instancias de un Asistente de tipo Becario
class AsistenteBecario: Asistente {
    
    var semestre : Int
    var idLineaBecas : Int
    var idFacultad : Int
    var idCarrera : Int
    
    /// Constructor único
    /// - Parameters:
    ///   - id: ID del asistente
    ///   - tipo: Tipo
    ///   - nombre: Nombre
    ///   - apellidoPaterno: Apellido paterno
    ///   - apellidoMaterno: Apellido materno
    ///   - correo: Correo
    ///   - telefonoParticular: Telefono particular
    ///   - telefonoCelular: Telefono celular
    ///   - semestre: Numero de semestre
    ///   - idLineaBecas: ID de la línea de becas que cursa
    ///   - idFacultad: ID de la Facultad en la que estudia
    ///   - idCarrera: ID de la Carrera que estudia
    init( id: Int, tipo: Int, nombre : String, apellidoPaterno : String, apellidoMaterno : String, correo : String, telefonoParticular : String, telefonoCelular : String, semestre : Int, idLineaBecas : Int, idFacultad : Int, idCarrera : Int) {
        
        self.semestre = semestre;
        self.idLineaBecas = idLineaBecas;
        self.idFacultad = idFacultad;
        self.idCarrera = idCarrera;
        
        super.init( id: id, tipo: tipo, nombre: nombre, apellidoPaterno: apellidoPaterno, apellidoMaterno: apellidoMaterno, correo: correo, telefonoParticular: telefonoParticular, telefonoCelular: telefonoCelular);
    }
    
    /// Funcion para convertir un Asistente Becario en un diccionario
    /// - Returns: El Asistente Becario en forma de diccionario
    override func toDictionary() -> [String : Any] {
        var dictionary = super.toDictionary()
        dictionary["semestre"] = semestre
        dictionary["idLineaBeca"] = idLineaBecas
        dictionary["idFacultad"] = idFacultad
        dictionary["idCarrera"] = idCarrera
        return dictionary
    }
    
}

// MARK: - Asistente externo
/// Subclase para modelar los objetos instancias de un Asistente de tipo Externo
class AsistenteExterno: Asistente {
    
    var procedencia : String
    
    /// Constructor único
    /// - Parameters:
    ///   - id: ID del asistente
    ///   - tipo: Tipo
    ///   - nombre: Nombre
    ///   - apellidoPaterno: Apellido paterno
    ///   - apellidoMaterno: Apellido materno
    ///   - correo: Correo
    ///   - telefonoParticular: Telefono particular
    ///   - telefonoCelular: Telefono celular
    ///   - procedencia: Procedencia
    init( id : Int, tipo : Int , nombre : String, apellidoPaterno : String, apellidoMaterno : String, correo : String, telefonoParticular : String, telefonoCelular : String, procedencia : String) {
        
        self.procedencia = procedencia

        super.init( id: id, tipo: tipo, nombre: nombre, apellidoPaterno: apellidoPaterno, apellidoMaterno: apellidoMaterno, correo: correo, telefonoParticular: telefonoParticular, telefonoCelular: telefonoCelular);
    }
    
    /// Funcion para convertir un Asistente Externo en un diccionario
    /// - Returns: El Asistente Externo en forma de diccionario
    override func toDictionary() -> [String : Any] {
        var dictionary = super.toDictionary()
        dictionary["procedencia"] = procedencia
        return dictionary
    }
    
}
