//
//  EvaluacionViewController.swift
//  Semana del Becario
//
//  Created by Comunicacion03 on 5/9/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit
import WebKit

class EvaluacionViewController: UIViewController {

    @IBOutlet var webView: WKWebView!
    let formulario = "https://forms.gle/CAvhuKJh3Ch6MkteA"
    
    
    /// Configuración del WebView
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self as? WKUIDelegate
        view = webView
    }
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Carga el WebView en el Outlet
        let myURL = URL(string: formulario)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
}
