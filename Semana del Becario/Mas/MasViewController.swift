//
//  MasViewController.swift
//  Semana del Becario
//
//  Created by Comunicacion03 on 5/5/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit


/// Controller para la seccion Mas
class MasViewController: UIViewController {

    // Lista de opciones
    var opciones: [Opcion] = []
    
    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        opciones = [Opcion(icono: "doc.text.fill", valor: "Evaluación"), Opcion(icono: "info.circle.fill", valor: "Acerca de")]
    }
}

extension MasViewController: UITableViewDataSource {
    
    // Numero de filas por sección
    // Secciones: 1, Filas: Tamaño de opcionesDict
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return opciones.count
    }
    
    // Asigna el contenido a cada celda dependiendo del indexPath
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Obtiene el valor de la opcion en la fila actual
        let opcion = opciones[indexPath.row]
        // Crea la celda, agrega el texto y la imagen
        let cell = UITableViewCell(style: .default, reuseIdentifier: "masCell")
        cell.imageView?.image = UIImage(systemName: opcion.icono)?.withTintColor(UIColor(named: "ColorPrimary")!)
        cell.textLabel?.text = opcion.valor
        return cell
    }
}

// MARK: - Table View Delegate
extension MasViewController: UITableViewDelegate {
    
    // Comportamiento cuando se selecciona una cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row{
            case 0:
                print("SegueEvaluacion")
                performSegue(withIdentifier: "segueEvaluacion", sender: nil)
            case 1:
            print("SegueContacto")
            performSegue(withIdentifier: "segueContacto", sender: nil)
            default:
                let alert = UIAlertController(title: "TODO", message: "Esta parte aún no se ha implementado", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
        }
    }
    
}
