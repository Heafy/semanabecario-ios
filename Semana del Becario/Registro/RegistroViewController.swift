//
//  SecondViewController.swift
//  Semana del Becario
//
//  Created by Comunicacion03 on 5/4/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit
import Eureka

class RegistroViewController: FormViewController {
    
    let nombreTag = "Nombre(s)*"
    let apPaternoTag = "Apellido Paterno*"
    let apMaternoTag = "Apellido Materno"
    let emailTag = "Email*"
    let telefonoTag = "Telefono*"
    let celulartag = "Celular*"
    let becarioTag = "Becario*"
    let facultadTag = "Facultad*"
    let carreraTag = "Carrera*"
    let semestreTag = "Semestre*"
    let lineaBecaTag = "Beca*"
    let procedenciaTag = "Procedencia*"
    
    var recursosLicenciatura: NSDictionary?
    var facultades = [String]()
    var carreras = [String]()
    var carrerasDict: NSDictionary?
    var becas = [String]()
    var semestres = [String]()
    
    // MARK: - View Lifecycle
    override func loadView() {
        super.loadView()
        poblarDatos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkforUserRegistered()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //checkforUserRegistered()
        
        // MARK: - Datos personales
        form +++ Section()
            <<< TextRow(nombreTag){
                $0.title = $0.tag
                $0.placeholder = $0.tag
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            <<< TextRow(apPaternoTag){
                $0.title = $0.tag
                $0.placeholder = $0.tag
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            <<< TextRow(apMaternoTag){
                $0.title = $0.tag
                $0.placeholder = $0.tag
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            <<< EmailRow(emailTag){
                $0.title = $0.tag
                $0.placeholder = $0.tag
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            <<< PhoneRow(telefonoTag){
                $0.title = $0.tag
                $0.placeholder = $0.tag
                $0.add(rule: RuleMinLength(minLength: 8))
                $0.add(rule: RuleMaxLength(maxLength: 12))
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            <<< PhoneRow(celulartag){
                $0.title = $0.tag
                $0.placeholder = $0.tag
                $0.add(rule: RuleMinLength(minLength: 10))
                $0.add(rule: RuleMaxLength(maxLength: 13))
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            
            // Switch para mostrar los datos de los Becarios
            +++ Section()
            <<< SwitchRow(becarioTag){
                $0.title = "Soy becario de la DGTIC"
                $0.value = false
            }
            // MARK: - Datos de becario
            +++ Section("Datos de Becarios"){
                $0.hidden = .function([self.becarioTag], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: self.becarioTag)
                    return row.value ?? false == false
                })
            }
            <<< PickerInputRow<String>(facultadTag){
                $0.title = $0.tag
                $0.options = facultades
                $0.value = $0.options.first
                // Regla que valida que el elemento seleccionado de la facultad no sea el primero
                let ruleRequired = RuleClosure<String> {
                    rowValue in
                    return (rowValue == self.facultades.first) ? ValidationError(msg: "Campo requerido") : nil
                }
                $0.add(rule: ruleRequired)
                $0.validationOptions = .validatesOnChange
            }
                // Actualiza el arreglo de las carreras cuando cambia
                .onChange({ (row) in
                    let secondRow = self.form.rowBy(tag: self.carreraTag) as! PickerInputRow<String>
                    let value = row.value!
                    let index = row.options.firstIndex(of: value)
                    // Evita errores cuando los optionals son nil
                    if let index = index, let _ = self.carrerasDict, let _ = self.carrerasDict?.object(forKey: "f\(index)"){
                        self.carreras = self.carrerasDict?.object(forKey: "f\(index)") as! [String]
                    } else {
                        self.carreras = []
                    }
                    secondRow.options = self.carreras
                    secondRow.value = self.carreras.first
                    secondRow.reload()
                })
            <<< PickerInputRow<String>(carreraTag){
                $0.title = $0.tag
                $0.options = carreras
                $0.value = $0.options.first
                // Regla que valida que el elemento seleccionado de la carrera no sea vacio
                let ruleRequired = RuleClosure<String> {
                    rowValue in
                    return (rowValue == nil) ? ValidationError(msg: "Campo requerido") : nil
                }
                $0.add(rule: ruleRequired)
                $0.validationOptions = .validatesOnChange
            }
            <<< PickerInputRow<String>(semestreTag){
                $0.title = $0.tag
                for i in 1...15{
                    semestres.append("\(i)º")
                }
                $0.options = semestres
                $0.value = $0.options.first
            }
            <<< PickerInputRow<String>(lineaBecaTag){
                $0.title = $0.tag
                $0.options = becas
                $0.value = $0.options.first
                // Regla que valida que el elemento seleccionado de la linea de becas no sea el primero
                let ruleRequired = RuleClosure<String> {
                    rowValue in
                    return (rowValue == self.becas.first) ? ValidationError(msg: "Campo requerido") : nil
                }
                $0.add(rule: ruleRequired)
                $0.validationOptions = .validatesOnChange
            }
            // MARK: - Datos de externo
            +++ Section(){
                $0.hidden = .function([self.becarioTag], { form -> Bool in
                    let row: RowOf<Bool>! = form.rowBy(tag: self.becarioTag)
                    return !(row.value ?? false == false)
                })
            }
            <<< TextRow(procedenciaTag){
                $0.title = $0.tag
                $0.placeholder = $0.tag
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            // MARK: - Boton registro
            +++ Section("* Campos requeridos")
            <<< ButtonRow("Registrarme") {
                $0.title = $0.tag
                $0.onCellSelection(self.buttonTapped)
                // Mantiene desactivado el boton mientras tenga campos inválidos
                $0.disabled = Condition.function(
                    form.allRows.compactMap { $0.tag }, // All row tags
                    { !$0.validate().isEmpty }) // Form has no validation errors
        }
    }
    
    /// Funcion para poblar los datos de licenciatura, facultades, carreras y becas a partir del archivo Info.plist
    func poblarDatos() {
        // Obtiene los datos del archivo licenciaturas.plist de facultades, carreras y líneas de becas
        if let path = Bundle.main.path(forResource: "licenciaturas", ofType: "plist") {
            recursosLicenciatura = NSDictionary(contentsOfFile: path)
            facultades = recursosLicenciatura?.object(forKey: "facultades") as! [String]
            carrerasDict = recursosLicenciatura?.object(forKey: "carreras") as? NSDictionary
            becas = recursosLicenciatura?.object(forKey: "becas") as! [String]
        }
    }
    
    /// Se ejecuta cuando se da click en el botón de Registro
    /// - Parameters:
    ///   - cell: Celda que invoca el método
    ///   - row: Fila donde vive la celda
    func buttonTapped(cell: ButtonCellOf<String>, row: ButtonRow) {
        debugPrint("Button register pressed")
        let valuesDictionary = form.values()
        let asistente = createAsistente(values: valuesDictionary)
        requestLogin(asistente: asistente)
    }
    
    /// Función para crear un asistente a partir de los parámetros recibidos
    /// - Parameter values: Diccionario de valores introducidos
    /// - Returns: El asistente creado a partir de los valores
    func createAsistente(values: [String:Any?]) -> Asistente {
        var asistente: Asistente?
        let nombre = values[nombreTag] as? String
        let apPaterno = values[apPaternoTag] as? String
        let apMaterno = values[apMaternoTag] as? String
        let email = values[emailTag] as? String
        let telefono = values[telefonoTag] as? String
        let celular = values[celulartag] as? String
        let esBecario: Bool? = values[becarioTag] as? Bool
        // Se verifica que el optional no sea nil
        if let esBecario = esBecario {
            if esBecario{
                let valuesBecario = getValuesBecario()
                asistente = AsistenteBecario(id: 0, tipo: 1, nombre: nombre!, apellidoPaterno: apPaterno!, apellidoMaterno: apMaterno!, correo: email!, telefonoParticular: telefono!, telefonoCelular: celular!, semestre: valuesBecario.semestre, idLineaBecas: valuesBecario.idLineaBecas, idFacultad: valuesBecario.idFacultad, idCarrera: valuesBecario.idCarrera)
            } else {
                let procedencia = values[procedenciaTag] as? String
                asistente = AsistenteExterno(id: 0, tipo: 2, nombre: nombre!, apellidoPaterno: apPaterno!, apellidoMaterno: apMaterno!, correo: email!, telefonoParticular: telefono!, telefonoCelular: celular!, procedencia: procedencia!)
            }
        }
        return asistente!
    }
    
    /// Función auxiliar para obtener los índices de los valores del becario
    /// - Returns: Una tupla de 4 valores con los índices de los valores introducidos por el Asistente
    func getValuesBecario() -> (semestre : Int, idLineaBecas : Int, idFacultad : Int, idCarrera : Int) {
        let facultadRow: PickerInputRow<String>? = form.rowBy(tag: facultadTag)
        let carreraRow: PickerInputRow<String>? = form.rowBy(tag: carreraTag)
        let semestreRow: PickerInputRow<String>? = form.rowBy(tag: semestreTag)
        let lineaBecaRow: PickerInputRow<String>? = form.rowBy(tag: lineaBecaTag)
        
        let facultadIndex = facultadRow?.options.firstIndex(of: (facultadRow?.value)!) ?? 0
        let carreraIndex = carreraRow?.options.firstIndex(of: (carreraRow?.value)!) ?? 0
        let semestreValue = Int(String((semestreRow?.value?.dropLast())!))
        let lineaBecaIndex = lineaBecaRow?.options.firstIndex(of: (lineaBecaRow?.value)!) ?? 0
        
        return (semestreValue!, lineaBecaIndex, facultadIndex, carreraIndex)
    }
    
    // MARK: - API Request
    
    /// Función que ejecuta el Post Request
    /// - Parameter asistente: El Asistente con sus valores en forma de diccionario
    func requestLogin(asistente: Asistente) {
        let asistenteDict = asistente.toDictionary()
        let session = URLSession.shared
        let requestURLString = APIService.registro
        guard let requestURL = URL(string: requestURLString) else { return }
        
        var dataArr = [String]()
        for(key, value) in asistenteDict {
            dataArr.append(key + "=\(value)")
        }
        let dataStr = dataArr.map { String($0) }.joined(separator: "&")
        let data = dataStr.data(using: .utf8)
        var postRequest = URLRequest(url: requestURL)
        postRequest.httpMethod = "POST"
        postRequest.httpBody = data
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: postRequest) {
            (data, response, error) in
            
            if let error = error {
                debugPrint("Error")
                debugPrint(error)
            }
            
            if let data = data, let response = response as? HTTPURLResponse {
                self.evaluateData(data: data, httpCode: response.statusCode, asistente: asistente)
            }
        }
        task.resume()
    }
    
    /// Función auxiliar para evaluar la respuesta del servidor
    /// Dependiendo el contenido envía la función correspondiente del presenter
    /// - Parameter data: Buffer de datos que contiene la respuesta del servidor
    func evaluateData(data: Data, httpCode: Int, asistente: Asistente) {
        debugPrint("Status code: \(httpCode)")
        if httpCode == 200 {
            let jsonResponse = try? JSONSerialization.jsonObject(with: data, options: [])
            if let dictionary = jsonResponse as? [String: Any] {
                guard let response = dictionary["response"] as? String else { return }
                guard let idAsistente = dictionary["idAsistente"] as? Double else { return }
//                print("ID Asistente: \(idAsistente)")
//                print("Nombre: \(asistente.nombre)")
//                print("Ap paterno: \(asistente.apellidoPaterno)")
//                print("Ap materno: \(asistente.apellidoMaterno)")
//                print("Correo: \(asistente.correo)")
                UserDefaults.standard.set(idAsistente, forKey: "idAsistente")
                UserDefaults.standard.set(asistente.nombre, forKey: "nombreAsistente")
                UserDefaults.standard.set(asistente.apellidoMaterno, forKey: "appPaternoAsistente")
                UserDefaults.standard.set(asistente.apellidoMaterno, forKey: "appMaternoAsistente")
                UserDefaults.standard.set(asistente.correo, forKey: "correoAsistente")
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Mensaje", message: response, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ir al programa", style: .default, handler: {
                        action in
                        self.routeToPrograma()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Aviso", message: "Hubo un error, por favor intenta mas tarde", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // MARK:- Check user
    
    
    /// Función para registrar y avisar cuando el usuario ya se ha registrado previamente
    func checkforUserRegistered() {
        let idAsistente = UserDefaults.standard.integer(forKey: "idAsistente")
        debugPrint("ID USER REGISTERED: \(idAsistente)")
        if idAsistente != 0 {
            let alert = UIAlertController(title: "Aviso", message: "Ya te has registrado", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ir al programa", style: UIAlertAction.Style.default, handler: {
                action in
                self.routeToPrograma()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK:- Redirect programa
    
    /// Función para redirigir al programa
    func routeToPrograma() {
        tabBarController?.selectedIndex = 0
    }
}
