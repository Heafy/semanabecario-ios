//
//  MisActividadesTableViewCell.swift
//  Semana del Becario
//
//  Created by Jorge Martinez on 04/10/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit

class MisActividadesTableViewCell: UITableViewCell {
    
    static let identifierCell = "misActividadesCell"

    @IBOutlet weak var eventoLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var horaLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Llena los datos de la Cell con la Actividad dada
    func poblar(with actividad: ActividadLista) {
        eventoLabel.text = actividad.nombreActividad
        fechaLabel.text = actividad.getDate()
        horaLabel.text = actividad.getTime()
    }
}
