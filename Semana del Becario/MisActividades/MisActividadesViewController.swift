//
//  MisActividadesViewController.swift
//  Semana del Becario
//
//  Created by Jorge Martinez on 03/10/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit

class MisActividadesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var actividadesList = [ActividadLista]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        actividadesList.removeAll()
        let id = UserDefaults.standard.integer(forKey: "idAsistente")
        debugPrint("ID USER REGISTERED: \(id)")
        requestActividades()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    // Prepare segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Segue para mostrar la entrada del asistente
        if segue.identifier == VerEntradaViewController.identifierSegue {
            let destination = segue.destination as! VerEntradaViewController
            destination.idActividad = sender as? Int
        }
    }
}

// MARK:- UITableViewDataSource
extension MisActividadesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return actividadesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MisActividadesTableViewCell.identifierCell, for: indexPath) as! MisActividadesTableViewCell
        let actividad = actividadesList[indexPath.row]
        cell.poblar(with: actividad)
        return cell
    }
}

// MARK:- UITableViewDelegate
extension MisActividadesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let actividad = actividadesList[indexPath.row]
        let idActividad = actividad.idActividad
        performSegue(withIdentifier: "segueVerQR", sender: idActividad)
    }
}

// MARK:- API Request
extension MisActividadesViewController {
    
    func requestActividades() {
        // Obtener id asistente
        let session = URLSession.shared
        let requestURLString = APIService.misActividades
        guard let requestURL = URL(string: requestURLString) else { return }
        var dataArr = [String]()
        let idAsistente = UserDefaults.standard.integer(forKey: "idAsistente")
        dataArr.append("id_asistente=\(idAsistente)")
        let dataStr = dataArr.map { String($0) }.joined(separator: "&")
        let data = dataStr.data(using: .utf8)
        var postRequest = URLRequest(url: requestURL)
        postRequest.httpMethod = "POST"
        postRequest.httpBody = data
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: postRequest) {
            (data, response, error) in
            
            if let error = error {
                debugPrint("Error")
                debugPrint(error)
            }
            
            if let data = data, let response = response as? HTTPURLResponse {
                if response.statusCode == 200 {
                    do {
                        //let actividadesList = try JSONDecoder().decode(MisActividades.self, from: data)
                        let json = try? JSONSerialization.jsonObject(with: data, options: [])
                        guard let mainArray = json as? [Any] else { return }
                        if mainArray.count > 1 {
                            guard let mainActividadesDict = mainArray[1] as? [String: Any] else { return }
                            guard let actividadesJSON = mainActividadesDict["actividades"] as? [Any] else { return }
                            for actividad in actividadesJSON {
                                let actividadJSON = try JSONSerialization.data(withJSONObject: actividad)
                                let decoder = JSONDecoder()
                                decoder.keyDecodingStrategy = .convertFromSnakeCase
                                let actividad = try decoder.decode(ActividadLista.self, from: actividadJSON)
                                self.actividadesList.append(actividad)
                            }
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                        }
                    }catch{
                        debugPrint("Error during JSON Data List Decoding: \(error)")
                    }
                }
            }
        }
        task.resume()
    }
}
