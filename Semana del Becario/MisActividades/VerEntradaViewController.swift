//
//  VerEntradaViewController.swift
//  Semana del Becario
//
//  Created by Jorge Martinez on 22/11/20.
//  Copyright © 2020 DGTIC. All rights reserved.
//

import UIKit

class VerEntradaViewController: UIViewController {

    @IBOutlet weak var nombreActividadLabel: UILabel!
    @IBOutlet weak var sedeLabel: UILabel!
    @IBOutlet weak var auditorioLabel: UILabel!
    @IBOutlet weak var qrLabel: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    
    var actividadActual: ActividadDetalle?
    var idActividad: Int?
    
    static let identifierSegue = "segueVerQR"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        super.loadView()
        debugPrint("ID Actividad: \(idActividad ?? 0)")
        requestActividad(idActividad: idActividad ?? 1) {
//            self.requestActividadesInscritas()
        }
    }
    
    func populate(actividad: ActividadDetalle) {
        let idAsistente = UserDefaults.standard.integer(forKey: "idAsistente")

        self.title = ""
        nombreActividadLabel.text = actividad.nombreActividad
        sedeLabel.text = actividad.nombreSede
        auditorioLabel.text = actividad.aula
        let hashids = Hashids(salt: "3qu1p0d1n4m1t4")
        let idActividadCifrado = hashids.encode(idActividad ?? 0) ?? ""
        let idAsistenteCifrado = hashids.encode(idAsistente) ?? ""
        let key = "\(idActividadCifrado)-\(idAsistenteCifrado)"
        qrLabel.text = key
        qrImageView.image = generarQR(from: key)
    }
}

// MARK: API Request
extension VerEntradaViewController {
    
    // Request datos de la actividad
    func requestActividad(idActividad: Int, _ completion:@escaping ()->()) {
        let session = URLSession.shared
        let requestURLString = APIService.detalleActividad
        guard let requestURL = URL(string: requestURLString + "\(idActividad)") else { return }
        var request = URLRequest(url: requestURL)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        // Closure para obtener los errores o los datos de la solicitud
        let task = session.dataTask(with: request, completionHandler: {
            (data, response, error) in
            if let error = error {
                debugPrint(error)
            }
            if let data = data {
                do {
                    self.actividadActual = try JSONDecoder().decode(ActividadDetalle.self, from: data)
                    DispatchQueue.main.async {
                        self.populate(actividad: self.actividadActual!)
                        completion()
                    }
                }catch{
                    debugPrint("Error during JSON Data List Decoding: \(error.localizedDescription)")
                }
            }
        })
        task.resume()
    }
}

// MARK: QR Image
extension VerEntradaViewController {
    
    func generarQR(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)

        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)

            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }

        return nil
    }
    
}

